using BioSequences
using CSV
using DataFrames
using CRISPRofftargetHunter

dir = dirname(abspath(Base.source_path()))
df = DataFrame(CSV.File(dir * "/../data/table_3_extended.csv"))
insertcols!(df, :COH_offt_aln => Vector{Union{String, Missing}}(missing, nrow(df)))
insertcols!(df, :COH_guide_aln => Vector{Union{String, Missing}}(missing, nrow(df)))
insertcols!(df, :COH_lev => Vector{Union{Int, Missing}}(missing, nrow(df)))
for r in eachrow(df)
    aln = CRISPRofftargetHunter.align(
        LongDNA{4}(reverse(String(r["target"][begin:end-3]))), 
        LongDNA{4}(reverse(String(r["offtarget_sequence_extended"][begin:end-3]))), 7)
    r["COH_offt_aln"] = String(reverse(aln.ref))
    r["COH_guide_aln"] = String(reverse(aln.guide))
    r["COH_lev"] = aln.dist + (r["target"][end] != r["offtarget_sequence_extended"][end]) +
        (r["target"][end-1] != r["offtarget_sequence_extended"][end-1])
end
CSV.write(dir * "/../data/table_3_extended_with_COH.csv", df)

df = DataFrame(CSV.File(dir * "/../data/table_6_extended.csv"))
insertcols!(df, :COH_offt_aln => Vector{Union{String, Missing}}(missing, nrow(df)))
insertcols!(df, :COH_guide_aln => Vector{Union{String, Missing}}(missing, nrow(df)))
insertcols!(df, :COH_lev => Vector{Union{Int, Missing}}(missing, nrow(df)))
for r in eachrow(df)
    aln = CRISPRofftargetHunter.align(
        LongDNA{4}(reverse(String(r["target"][begin:end-3]))), 
        LongDNA{4}(reverse(String(r["offtarget_sequence_extended"][begin:end-3]))), 7)
    r["COH_offt_aln"] = String(reverse(aln.ref))
    r["COH_guide_aln"] = String(reverse(aln.guide))
    r["COH_lev"] = aln.dist + (r["target"][end] != r["offtarget_sequence_extended"][end]) +
        (r["target"][end-1] != r["offtarget_sequence_extended"][end-1])
end
CSV.write(dir * "/../data/table_6_extended_with_COH.csv", df)
