rm(list=ls(all = TRUE))
gc(reset=TRUE)

library(data.table)
library(ggplot2)
library(ggthemes)
library(patchwork)
library(stringr)

dt <- fread("data/all_combined.csv")
dt$PAM <- str_sub(dt$offtarget_sequence_extended, start = -2)
dt <- dt[dt$PAM == "GG"]
dt <- dt[distance != COH_lev][!is.na(GUIDE_rescaled)]
dt2 <- melt(dt, id.vars = c("GUIDE_rescaled"), 
            measure.vars = c("distance", "COH_lev"), variable.name = "Distance")
dt2$Distance <- factor(dt2$Distance, levels = c("distance", "COH_lev"), labels = c("Hamming", "Levenshtein"))

p <- ggplot(dt2, aes(x = as.factor(value), y = GUIDE_rescaled)) +
  geom_boxplot() +
  geom_violin(alpha = 0.5) +
  scale_y_log10() +
  labs(x = "Distance", y = "GUIDE-seq indel frequency") +
  theme_pander() +
  theme_pander(base_size = 14) +
  facet_grid(. ~ Distance)
p

ggsave("figures/levenshtein_vs_hamming_violins.png", p, dpi = 300)

# show in some drastic way that after correcting distance 
# there is big change in CHANGE-seq counts per distance and it 
# is more consistent

